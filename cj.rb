require 'nokogiri'
require 'open-uri'

def discover_search_path(url)
    #trim trailing '/' from url
    url.gsub!(/\/\Z/,'')
    
    page = Nokogiri::HTML(open(url))
	form = page.css('form[method=get]')
    search_path = ''

    # if there are multiple forms on the page
    # grab the first one that contains the word 'search'
    if form.count > 1
        form.each{ |f|
            if f.include? "search"
                form = f
                break
            end
        }
    else
        form = form[0]
    end

    # set search path to forms action parameter
    search_path = form['action']

    # remove leading or trailing slashes
    search_path.gsub!(/\A\/|\/\Z/,'')

    # prepend url to search_path if it isn't there already
    search_path = url + '/' + search_path unless search_path.include? url


    # get form inputs of type 'search' or 'text'
    form_inputs = form.css('input[type=search]')
    if form_inputs.empty?
       form_inputs = form.css('input[type=text]')
    end

    search_path + '/?' + form_inputs[0]['name'] + '='
end


def search_for_item(item,url) 
	search_path = discover_search_path(url) + item

	Nokogiri::HTML(open(search_path))
end

print discover_search_path('https://kat.cr')
# search_for_item('metallica','https://kat.cr')
